from django.db import models
from django.utils import timezone
import pytz

# Create your models here.
class Message(models.Model):
	def converTZ():
		return timezone.now() + timezone.timedelta(hours=0)

	name = models.CharField(max_length=27)
	email = models.EmailField()
	message = models.TextField()
	created_date = models.DateTimeField(default=converTZ)

	def __str__(self):
		return self.message

    