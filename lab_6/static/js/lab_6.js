//Theme
$(document).ready(function() {
    theme = [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}];

    localStorage.setItem("theme", JSON.stringify(theme));

    select = $('.my-select').select2();

    select.select2({
      'data': JSON.parse(localStorage.getItem("theme"))
    });

    var arraysTheme = JSON.parse(localStorage.getItem("theme"));
    var indigoColor = arraysTheme[3]; //defaultTheme
    var defaultColor = indigoColor;
    var selectedColor = defaultColor;
    var oldColor = defaultColor;

  
    if (localStorage.getItem("selectedColor") !== null) {
        oldColor = JSON.parse(localStorage.getItem("selectedColor"));
    }


    selectedColor = oldColor;

    $('body').css(
    {
      "background-color" : selectedColor.bcgColor,
      "font-color" : selectedColor.fontColor
    }
    );

    var arrow = "down";

    $('#image').click(function(){
      $('.chat-body').toggle();
      if(arrow === "down"){
        $('#image').attr({src: "https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_up-16.png"});
        arrow = "up";
      }

      else{
       $('#image').attr({src: "https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_down-16.png"});
        arrow = "down"; 

      }
    });


    $('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
      var themeChoosed = select.val();
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada

      if(themeChoosed < arraysTheme.length){
        selectedColor = arraysTheme[themeChoosed];
      }

    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
      $('body').css({

        "background-color" : selectedColor.bcgColor,
        "font-color" : selectedColor.fontColor
      }

      );
    // [TODO] simpan object theme tadi ke local storage selectedTheme
      localStorage.setItem("selectedColor",JSON.stringify(selectedColor));
    })
});


// ChatBox
document.getElementById("button-send").addEventListener("click",display);

function display(){
  var d = new Date();
  var hour = d.getHours();
  var minute = d.getMinutes();
  hour = check(hour);
  minute = check(minute);
  var time = hour + ":" + minute;
  var word = document.getElementById('write').value;
  document.getElementById('chat').innerHTML= document.getElementById('chat').innerHTML + word + " " +'('+time+')'+ '<br>';
  document.getElementById('write').value = "";
}

function check(i){
  if(i<10){i= "0"+i};
  return i;
}


// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = "";
  }

  else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } 
  
  else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}